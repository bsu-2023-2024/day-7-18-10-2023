#include <iostream>

void typeOfSequence(int n, int length);
int getLength(int n);

using namespace std;

int main()
{
    cout << "Enter any integer number: ";
    int number;
    cin >> number;

    typeOfSequence(abs(number), getLength(abs(number)));
}

int getLength(int n) {
    int i = 0;

    while (n > 0)
    {
        n = n / 10;
        i++;
    }
    return i;
}

void typeOfSequence(int number, int length) {

    bool up = 0, equal = 0, down = 0;
    int power = pow(10, length - 1);
    for (int i = 1; i < length; i++)
    {
        int first = number / power;
        int second = (number / (power / 10)) % 10;
        if (first < second)
            up = true;
        if (first == second)
            equal = true;
        if (first > second)
            down = true;
        number %= power;
        power /= 10;
    }

    if (down && !up)
        if (equal)
            cout << "Not strictly decreasing sequence";
        else
            cout << "Strictly decreasing sequence";
    else if (up && !down)
    {
        if (equal)
            cout << "Not strictly increasing sequence";
        else
            cout << "Strictly increasing sequence";
    }
    else if (equal)
        cout << "Monotonic sequence";
    else
        cout << "Unordered secuence";
}
