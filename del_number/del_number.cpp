#include <iostream>

int delNumber(long long n, int length, int del);
int getLength(long long n);

using namespace std;

int main()
{
    long long n;
    int del;
    cout << "Enter integer number: ";
    cin >> n;
    cout << "Enter number to delete: ";
    cin >> del;

    int new_n = delNumber(n, getLength(n), del);
    if (!new_n)
    {
        cout << "Selected number is not in sequence";
    }
    else
    {
        cout << new_n;
    }
 
}

int getLength(long long n) {
    int i = 0;
    while (n > 0)
    {
        i++;
        n /= 10;
    }
    return i;
}

int delNumber(long long n, int length, int del) {

    int add = 0;
    for (int i = 0; i < length; i++)
    {
        if(n % 10 == del)
        {
            n = n / 10;
            i--;
        }
        else
        {
            add += (n % 10) * pow(10, i);
            n = n / 10;
        }
    }
    return n + add;
}
